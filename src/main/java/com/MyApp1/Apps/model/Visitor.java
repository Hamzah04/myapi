package com.MyApp1.Apps.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "VISITOR")
public class Visitor {
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "UMUR")
    private Integer umur;
    @Column(name = "ALAMAT")
    private String alamat;
    @Column(name = "TEMPAT_LAHIR")
    private String tempatLahir;
    // @Column(name = "TGL_LAHIR")
    // private String tgl_lahir;


    public Visitor() {
    }

    public Visitor(Integer id, String name, String email, Integer umur, String alamat, String tempatLahir) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.umur = umur;
        this.alamat = alamat;
        this.tempatLahir = tempatLahir;
        // this.tgl_lahir = tgl_lahir;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUmur() {
        return umur;
    }

    public void setUmur(Integer umur) {
        this.umur = umur;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    // public String getTgl_lahir() {
    //     return tgl_lahir;
    // }

    // public void setTgl_lahir(String Tgl_lahir) {
    //    this.tgl_lahir = tgl_lahir;
    // }
}

