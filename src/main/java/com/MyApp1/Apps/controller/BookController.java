package com.MyApp1.Apps.controller;

import com.MyApp1.Apps.model.Book;
import com.MyApp1.Apps.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class BookController {
    //komentar 1
    //komentar 2


    @Autowired
    public BookService service;

    @GetMapping("/book")
    public List<Book> list() {
        return service.listAll();
    }

    @GetMapping("/book/{id}")
    public ResponseEntity<Book> get(@PathVariable Integer id) {
        try {
            Book books = service.get(id);
            return new ResponseEntity<Book>(books, HttpStatus.OK);
        }catch (NoSuchElementException e) {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/book")
    public String add(@RequestBody Book books) {
        service.save(books);
        return "ADD BOOK SUCCESS";
    }

    @PutMapping("/book/{id}")
    public ResponseEntity<?> update(@RequestBody Book books, @PathVariable Integer id){
        try {
            service.get(id);
            service.save(books);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
    }

    @DeleteMapping("/book/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);

    }

}
