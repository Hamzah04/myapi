package com.MyApp1.Apps.controller;

import java.util.List;

import com.MyApp1.Apps.model.Login;
import com.MyApp1.Apps.service.LoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    

    @Autowired
    public LoginService service;

    @GetMapping("/login")
    public List<Login> list() { return service.listAll(); }

}
