package com.MyApp1.Apps.controller;

import com.MyApp1.Apps.model.User;
import com.MyApp1.Apps.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class UserController {

    @Autowired
    public UserService service;

    @GetMapping("/user")
    public List<User> list() { return service.listAll(); }


    @GetMapping("/user/{id}")
    public ResponseEntity<User> get(@PathVariable Integer id) {
        try {
            User users = service.get(id);
            return new ResponseEntity<User>(users, HttpStatus.OK);
        }catch (NoSuchElementException e) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/user")
    public String add(@RequestBody User users) {
        service.save(users);
        return "ADD USER SUCCESS";
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<?> update(@RequestBody User users, @PathVariable Integer id){
        try {
            service.get(id);
            service.save(users);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
    }

    @DeleteMapping("/user/{id}")
    public void delete(@PathVariable Integer id) {

    }



}
