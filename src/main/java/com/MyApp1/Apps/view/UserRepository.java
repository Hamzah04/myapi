package com.MyApp1.Apps.view;

import com.MyApp1.Apps.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
