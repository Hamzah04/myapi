package com.MyApp1.Apps.view;

import com.MyApp1.Apps.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Integer> {
}
