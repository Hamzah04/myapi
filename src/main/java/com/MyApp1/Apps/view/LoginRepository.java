package com.MyApp1.Apps.view;

import com.MyApp1.Apps.model.Login;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginRepository extends JpaRepository<Login, Integer> {
}