package com.MyApp1.Apps.service;

import java.util.List;

import com.MyApp1.Apps.model.Login;
import com.MyApp1.Apps.view.LoginRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    
    @Autowired
    private LoginRepository repo;

    public List<Login> listAll() {
        return repo.findAll();
    }

    public void save(Login logins) {
        repo.save(logins);
    }

    public Login get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) { repo.deleteById(id);}
}
