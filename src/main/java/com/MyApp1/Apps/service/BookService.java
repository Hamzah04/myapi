package com.MyApp1.Apps.service;

import com.MyApp1.Apps.model.Book;
import com.MyApp1.Apps.view.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    public BookRepository repo;

    public List<Book> listAll(){
       return repo.findAll();
    }

    public void save(Book books) {
        repo.save(books);
    }

    public Book get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
