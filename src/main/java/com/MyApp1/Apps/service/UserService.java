package com.MyApp1.Apps.service;

import com.MyApp1.Apps.model.User;
import com.MyApp1.Apps.view.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repo;

    public List<User> listAll() {
        return repo.findAll();
    }

    public void save(User users) {
        repo.save(users);
    }

    public User get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) { repo.deleteById(id);}
}
